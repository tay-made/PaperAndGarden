import React from 'react';

const Footer = () => {
  return (
    <div id="footer">
      <div id="buttonNavigation">

        <span id="linktreeButton">
          <span className="buttonHoverText">Writing Resources</span>
        </span>

        <span id="githubButton">
          <span className="buttonHoverText">Github Repo</span>
        </span>

        <span id="tayMadeButton">
          <span className="buttonHoverText">Portfolio</span>
        </span>

        <span id="linkedInButton">
          <span className="buttonHoverText">Linked In</span>
        </span>

        <span id="contactButton">
          <span className="buttonHoverText">Contact</span>
        </span>

      </div>
    </div>
  )
}

export default Footer;